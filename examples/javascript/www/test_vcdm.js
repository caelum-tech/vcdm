import {
    VCredential,
    VPresentation
} from 'vcdm';

console.log("*****************************");
console.log("            VCDM            ");
console.log("*****************************");

console.log("*****************************");
console.log("          CREDENTIAL         ");
console.log("*****************************");
// Contructing a Verifiable Credential
let vc = new VCredential("my_id")
    // Setters overwrite values of property
    .setIssuanceDate("2010-01-01T19:73:24Z")
    .setIssuer("Issuer")
    .setCredentialSubject({
        "type": "parental_admission",
        "name": "parent_name",
        "field_trip": "Sagrada Familia",
        "permission": "true",
    })
    // Setting another credential totally different and defined by user
    .addCredentialSubject({
        "type": "my_id",
        "name": "my_name",
        "mnumber": "my_number",
        "address": "my_address",
        "birthDate": "my_birth_date",
    })
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    })
    // Adders push values to the vector properties
    .addContext("added context")
    .addType("added type");

console.log("Verifiable Credential:", vc.toJSON());

console.log("Constructing a VerifiableCredential all at once:");

let vc_json = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": "http://example.com/credentials/4643",
    "type": [
        "VerifiableCredential",
        "PersonalInformation"
    ],
    "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "issuanceDate": "2010-01-01T19:73:24Z",
    "credentialStatus": {
        "id": "StatusID",
        "type": "available"
    },
    "credentialSubject": [{
        "type": "did:example:abfab3f512ebc6c1c22de17ec77",
        "name": "Mr John Doe",
        "mnumber": "77373737373A",
        "address": "10 Some Street, Anytown, ThisLocal, Country X",
        "birthDate": "1982-02-02-00T00:00Z"
    }],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let ver_cred = VCredential.fromJSON(vc_json);

console.log(ver_cred.toJSON());

console.log("*****************************");
console.log("         PRESENTATION        ");
console.log("*****************************");
// Contructing a Verifiable Presentation
let vp = new VPresentation("my_id")
    .addContext("added Context")
    .addType("added type")
    // Add the created Verifiable Credential
    .addVerifiableCredential(vc.toJSON())
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });

console.log("Verifiable Presentation:", vp.toJSON());

console.log("Constructing a VerifiablePresentation all at once:");

let vp_json = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "type": [
        "VerifiableCredential",
        "PersonalInformation"
    ],
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://www.w3.org/2018/credentials/examples/v1"
            ],
            "id": "http://example.com/credentials/4643",
            "type": [
                "VerifiableCredential",
                "PersonalInformation"
            ],
            "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "issuanceDate": "2010-01-01T19:73:24Z",
            "credentialStatus": {
                "id": "StatusID",
                "type": "available"
            },
            "credentialSubject": [{
                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                "name": "Mr John Doe",
                "mnumber": "77373737373A",
                "address": "10 Some Street, Anytown, ThisLocal, Country X",
                "birthDate": "1982-02-02-00T00:00Z"
            }],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            }
        }
    ],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let ver_pres = VPresentation.fromJSON(vp_json);

console.log(ver_pres.toJSON());
