import { DidDoc } from 'diddoc';

console.log("*****************************");
console.log("           DIDDOC            ");
console.log("*****************************");

let d = new DidDoc("my_context", "my_id")
    .setContext("Context2")
    .addAuthentication({
        "type": "Ed25519SignatureAuthentication2018",
        "publicKey": [
            "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
            "did:sov:WRfXPg8dantKVubE3HX8pw#key-2"
        ]
    })
    .addPublicKey({
        "id": "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
        "type": "Ed25519VerificationKey2018",
        "owner": "",
        "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    })
    .addService({
        "id": "did:ser:18y34b91c612c4123gwrh45y",
        "type": "agent",
        "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    })
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    })
    .setCreated("2002-10-10T17:00:00Z")
    .setUpdated("2016-10-17T02:41:00Z");

console.log(d.toJSON());

console.log("*****************************");
console.log("           GETTERS           ");
console.log("*****************************");

console.log("Authentication:", d.getAuthentication());
console.log("Context:", d.getContext());
console.log("ID:", d.getId());
console.log("Public Key:", d.getPublicKey());
console.log("Services:", d.getService());
console.log("Proof:", d.getProof());
console.log("Created:", d.getCreated());
console.log("Updated:", d.getUpdated());