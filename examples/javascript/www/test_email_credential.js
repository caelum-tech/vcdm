import { DidDoc } from 'diddoc';
import { VCredential } from 'vcdm';

let now = new Date(Date.now()).toISOString();

// Create DID and DID document
console.log("*****************************");
console.log("           DIDDOC            ");
console.log("*****************************");

let d = new DidDoc("https://www.w3.org/2018/credentials/v1", "my_id")
    .setContext("https://www.w3.org/2018/credentials/examples/v1")
    .addAuthentication({
      "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
      "type": "OpenIdConnectVersion1.0Service",
      "owner": "OWNER!",
      "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    })
    .addAuthentication("did:lrn:WRfXPg8dantKVubE3HX8pw#key-1")
    .setPublicKey({
      "id": "did:lrn:WRfXPg8dantKVubE3HX8pw#key-1",
      "type": "Ed25519VerificationKey2018",
      "owner": "my_id",
      "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    })
    .setCreated(now)
    .setUpdated(now);

console.log(d.toJSON());

let vc = new VCredential("my_id")
    .setIssuanceDate(now)
    .setIssuer("did:lrn:123124212#caelum-identity")
    .setCredentialSubject({
      "type": "EmailCredential",
      "email": "example@email.com",
    })
    .setProof({
      "type": "RsaSignature2018",
      "created": now,
      "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
      "signatureValue": "pY9...Cky6Ed = "
    })
    .addProof({
      "type": "RsaSignature2018",
      "created": now,
      "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
      "signatureValue": "pY9...Cky6Ed = "
    })
    .addType("EmailCredential");

console.log("Verifiable Credential:", vc.toJSON());