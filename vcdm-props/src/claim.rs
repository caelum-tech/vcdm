use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

// Claims consist of a *subject-property-value* relationship

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Subject {
    subject: String,
}

impl From<String> for Subject {
    fn from(s: String) -> Self {
        Subject { subject: s }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Property {
    property: String,
}

impl From<String> for Property {
    fn from(s: String) -> Self {
        Property { property: s }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Value {
    value: String,
}

impl From<String> for Value {
    fn from(s: String) -> Self {
        Value { value: s }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Claim {
    #[serde(flatten)]
    subject: Subject,
    #[serde(flatten)]
    property: Property,
    #[serde(flatten)]
    value: Value,
}

impl Claim {
    pub fn empty_claim() -> Self {
        Claim {
            subject: Subject::from("".to_string()),
            property: Property::from("".to_string()),
            value: Value::from("".to_string()),
        }
    }

    pub fn new(subject: String, property: String, value: String) -> Self {
        Claim {
            subject: Subject::from(subject),
            property: Property::from(property),
            value: Value::from(value),
        }
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn serde_claim() {
        let should = Claim::new(
            "Pat".to_string(),
            "alumniOf".to_string(),
            "Example University".to_string(),
        );
        let c: Claim = serde_json::from_str(
            &r#"
        {
            "subject": "Pat",
            "property": "alumniOf",
            "value": "Example University"
        }
        "#,
        )
        .unwrap();
        assert_eq!(
            serde_json::to_string(&should).unwrap(),
            serde_json::to_string(&c).unwrap()
        );
    }
}
