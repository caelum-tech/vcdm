use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Proof {
    #[serde(rename(serialize = "type", deserialize = "type"))]
    auth_type: String,
    created: String,
    #[serde(rename(serialize = "verificationMethod", deserialize = "verificationMethod"))]
    id: String,
    #[serde(rename(serialize = "signatureValue", deserialize = "signatureValue"))]
    pubkey: String,
}

impl Proof {
    pub fn empty_proof() -> Self {
        Proof {
            auth_type: "".to_string(),
            created: "".to_string(),
            id: "".to_string(),
            pubkey: "".to_string(),
        }
    }

    pub fn new(auth_type: String, created: String, id: String, pubkey: String) -> Self {
        Proof {
            auth_type,
            created,
            id,
            pubkey,
        }
    }

    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}
