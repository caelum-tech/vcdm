/*
use wasm_bindgen::prelude::*;
use serde::ser::{Serialize, Serializer};

use public_key::PublicKey;

#[wasm_bindgen]
pub enum VerificationMethods {
    Ref(String),
    Embedded(PublicKey),
}

#[wasm_bindgen]
impl VerificationMethods {
    pub fn validate_verification_method(_a: &VerificationMethods) -> bool {
        true
    }

    pub fn new_ref(id: String) -> Self {
        VerificationMethods::Ref(id)
    }

    pub fn new_embedded(p: PublicKey) -> Self {
        VerificationMethods::Embedded(p)
    }
}

impl Serialize for VerificationMethods {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            VerificationMethods::Ref(id) => serializer.serialize_str(id),
            VerificationMethods::Embedded(pk) => pk.serialize(serializer),
        }
    }
}
*/
