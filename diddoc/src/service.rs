use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Service {
    id: String,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    service_type: String,
    #[serde(rename(serialize = "serviceEndpoint", deserialize = "serviceEndpoint"))]
    endpoint: String,
}

impl Service {
    pub fn new(id: String, service_type: String, endpoint: String) -> Service {
        Service {
            id,
            service_type,
            endpoint,
        }
    }

    pub fn from_json(json: JsValue) -> Service {
        json.into_serde().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_service() {
        let should = Service::new(
            "did:ser:18y34b91c612c4123gwrh45y".to_string(),
            "service_type".to_string(),
            "www.caelumlabs.com/api/".to_string(),
        );
        let serv: Service = serde_json::from_str(
            &r#"
        {
            "id": "did:ser:18y34b91c612c4123gwrh45y",
            "type": "service_type",
            "serviceEndpoint": "www.caelumlabs.com/api/"
        }
        "#,
        )
        .unwrap();
        assert_eq!(
            serde_json::to_string(&should).unwrap(),
            serde_json::to_string(&serv).unwrap()
        );
    }
}
