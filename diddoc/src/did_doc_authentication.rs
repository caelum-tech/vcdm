use serde::{Deserialize, Serialize};

use crate::did_doc_public_key::DidDocPublicKey;
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum Authentication {
    DID(String),
    PublicKey(DidDocPublicKey),
}

impl Authentication {
    pub fn from_json(json: JsValue) -> Authentication {
        json.into_serde().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_enum_serde() {
        let auth_string = Authentication::DID("did:lrn:123412341234".to_string());
        assert_eq!(
            serde_json::to_string(&auth_string).unwrap(),
            serde_json::to_string("did:lrn:123412341234").unwrap()
        );
        let should_pk: DidDocPublicKey = serde_json::from_str(
            r#"{
            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "type": "OpenIdConnectVersion1.0Service",
            "owner": "OWNER!",
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
        }"#,
        )
        .unwrap();

        let auth_public_key = Authentication::PublicKey(should_pk.clone());
        assert_eq!(
            serde_json::to_string(&auth_public_key).unwrap(),
            serde_json::to_string(&should_pk).unwrap(),
        );
    }
}
