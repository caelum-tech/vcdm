use serde::de::{self, Deserialize, Deserializer, MapAccess, SeqAccess, Visitor};
use serde::ser::{Serialize, SerializeMap, Serializer};
use std::fmt;
use wasm_bindgen::JsValue;

#[derive(Debug, Clone)]
pub struct DidDocPublicKey {
    id: String,
    pk_type: String,
    owner: String,
    property: String,
    property_val: String,
}

impl DidDocPublicKey {
    pub fn new(
        id: String,
        pk_type: String,
        owner: String,
        property: String,
        property_val: String,
    ) -> Self {
        DidDocPublicKey {
            id,
            pk_type,
            owner,
            property,
            property_val,
        }
    }

    pub fn from_json(json: JsValue) -> DidDocPublicKey {
        json.into_serde().unwrap()
    }

    pub fn set_id(&mut self, id: String) {
        self.id = id;
    }

    pub fn get_id(&self) -> &String {
        &self.id
    }

    pub fn set_type(&mut self, pk_type: String) {
        self.pk_type = pk_type;
    }

    pub fn get_type(&self) -> &String {
        &self.pk_type
    }

    pub fn set_owner(&mut self, owner: String) {
        self.owner = owner;
    }

    pub fn get_owner(&self) -> &String {
        &self.owner
    }

    pub fn set_property(&mut self, property: String) {
        self.property = property;
    }

    pub fn get_property(&self) -> &String {
        &self.property
    }

    pub fn set_property_val(&mut self, property_val: String) {
        self.property_val = property_val;
    }

    pub fn get_property_val(&self) -> &String {
        &self.property_val
    }
}

impl Serialize for DidDocPublicKey {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_map(Some(5))?;
        state.serialize_entry("owner", &self.owner)?;
        state.serialize_entry("id", &self.id)?;
        state.serialize_entry("type", &self.pk_type)?;
        state.serialize_entry(&self.property, &self.property_val)?;
        state.end()
    }
}

impl<'de> Deserialize<'de> for DidDocPublicKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        enum Field {
            Id,
            Type,
            Owner,
            Property(String),
        };

        impl<'de> Deserialize<'de> for Field {
            fn deserialize<D>(deserializer: D) -> Result<Field, D::Error>
            where
                D: Deserializer<'de>,
            {
                struct FieldVisitor;

                impl<'de> Visitor<'de> for FieldVisitor {
                    type Value = Field;

                    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                        formatter.write_str("`id` or `type` or `owner` or `WhatEver`")
                    }

                    fn visit_str<E>(self, value: &str) -> Result<Field, E>
                    where
                        E: de::Error,
                    {
                        match value {
                            "id" => Ok(Field::Id),
                            "type" => Ok(Field::Type),
                            "owner" => Ok(Field::Owner),
                            s => Ok(Field::Property(s.to_string())),
                        }
                    }
                }

                deserializer.deserialize_identifier(FieldVisitor)
            }
        }
        struct DidDocPublicKeyVisitor;

        impl<'de> Visitor<'de> for DidDocPublicKeyVisitor {
            type Value = DidDocPublicKey;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct Duration")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<DidDocPublicKey, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let mut f = |i: usize| -> Result<String, V::Error> {
                    seq.next_element()?
                        .ok_or_else(|| de::Error::invalid_length(i, &self))
                };
                let id = f(0)?;
                let pk_type = f(1)?;
                let owner = f(2)?;
                let property = f(3)?;
                let property_val = f(4)?;

                Ok(DidDocPublicKey::new(
                    id,
                    pk_type,
                    owner,
                    property,
                    property_val,
                ))
            }

            fn visit_map<V>(self, mut map: V) -> Result<DidDocPublicKey, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut id = None;
                let mut pk_type = None;
                let mut owner = None;
                let mut property = None;
                let mut property_val = None;

                macro_rules! vis {
                    ($k:expr, $v:expr) => {{
                        if $k.is_some() {
                            return Err(de::Error::duplicate_field($v));
                        }
                        $k = Some(map.next_value()?);
                    }};
                }
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Id => vis!(id, "id"),
                        Field::Type => vis!(pk_type, "pk_type"),
                        Field::Owner => vis!(owner, "owner"),
                        Field::Property(s) => {
                            if property.is_some() {
                                return Err(de::Error::duplicate_field("property"));
                            }
                            property = Some(s);
                            property_val = Some(map.next_value()?);
                        }
                    }
                }

                let id = id.ok_or_else(|| de::Error::missing_field("id"))?;
                let pk_type = pk_type.ok_or_else(|| de::Error::missing_field("pk_type"))?;
                let owner = owner.ok_or_else(|| de::Error::missing_field("owner"))?;
                let property = property.ok_or_else(|| de::Error::missing_field("property"))?;
                let property_val =
                    property_val.ok_or_else(|| de::Error::missing_field("property_val"))?;
                Ok(DidDocPublicKey::new(
                    id,
                    pk_type,
                    owner,
                    property,
                    property_val,
                ))
            }
        }

        const FIELDS: &'static [&'static str] =
            &["id", "pk_type", "owner", "property", "property_val"];
        deserializer.deserialize_struct("DidDocPublicKey", FIELDS, DidDocPublicKeyVisitor)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_diddoc_publickey() {
        let should: DidDocPublicKey = DidDocPublicKey::new(
            "did:example:ebfeb1276e12ec21f712ebc6f1c".to_string(),
            "OpenIdConnectVersion1.0Service".to_string(),
            "OWNER!".to_string(),
            "PublicKeyBase58".to_string(),
            "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV".to_string(),
        );

        let dd_pk: DidDocPublicKey = serde_json::from_str(
            &r#"
        {
            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "type": "OpenIdConnectVersion1.0Service",
            "owner": "OWNER!",
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
        }
        "#,
        )
        .unwrap();

        assert_eq!(
            serde_json::to_string(&should).unwrap(),
            serde_json::to_string(&dd_pk).unwrap()
        );
        assert_eq!(should.get_id(), dd_pk.get_id());
        assert_eq!(should.get_type(), dd_pk.get_type());
        assert_eq!(should.get_owner(), dd_pk.get_owner());
        assert_eq!(should.get_property(), dd_pk.get_property());
        assert_eq!(should.get_property_val(), dd_pk.get_property_val());
    }
}
