//! # **D**ecentralized **Id**entity **Doc**uments
//!
//! The `diddoc` crate is aimed to help `javascript` and `rust` developers manage `DID docs`
//! (**D**ecentralized **Id**entity **Doc**uments) in an easy manner.
//! Following [w3's specifications](https://w3c-ccg.github.io/did-spec/#did-documents) on DID
//! Documents this crate aims to be a [Resolver](https://w3c-ccg.github.io/did-spec/#did-resolvers).
//! This means it will create, read, update and delete all properties of a DID document. As well as
//! interacting with other DIDs and DID document, using proof verification.
//! Also, it will be able to interact with others on other Decentralized Identifier Registry.
//! This will can effectively be used to perform all the operations required of a CKMS
//! (cryptographic key management system), such as key registration, replacement, rotation,
//! recovery, and expiration.
//! More details in [w3's method specifications](https://w3c-ccg.github.io/did-spec/#did-methods).
//!
#[cfg(feature = "serde-serialize")]
extern crate serde;

extern crate serde;

extern crate serde_json;
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

use crate::did_doc_authentication::Authentication;
use crate::did_doc_public_key::DidDocPublicKey;
use crate::did_document::DidDocument;
use crate::proof::Proof;
use crate::service::Service;

pub mod did_doc_authentication;
pub mod did_doc_public_key;
pub mod did_document;
pub mod proof;
pub mod service;

#[wasm_bindgen]
pub struct DidDoc {
    ctx: JsValue,
}

#[wasm_bindgen]
impl DidDoc {
    // CONSTRUCTORS
    /// # Web assembly Constructor
    /// `DidDoc`'s constructor will create an empty `DidDoc` object. An empty object
    /// is not a valid `DidDoc`.
    /// ```javascript
    /// import { DidDoc } from 'diddoc';
    /// let d = new DidDoc("my_context", "my_id");
    /// ```
    ///
    #[wasm_bindgen(constructor)]
    pub fn new(context: String, id: String) -> DidDoc {
        let d = DidDocument::new(context, id);
        DidDoc { ctx: d.to_json() }
    }

    /// # WebAssembly Constructor `formJSON`
    /// `DidDoc`'s constructor will create a `DidDoc` object from input. Input must be a
    /// JSON object with all properties defined. If no value is wanted for certain property, must
    /// be empty. This is also true for sub properties (properties of properties).
    /// ```javascript
    /// import { DidDoc } from 'diddoc';
    /// let myDidDoc_json = {
    ///    "@context": [
    ///        "https://www.w3.org/2018/credentials/v1",
    ///        "https://www.w3.org/2018/credentials/examples/v1"
    ///    ],
    ///    "id": "http://example.com/credentials/4643",
    ///    "authentication": [
    ///         {
    ///            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "type": "OpenIdConnectVersion1.0Service",
    ///            "owner": "OWNER!",
    ///            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///         },
    ///         "did:lrn:123412341234#my_id"
    ///     ],
    ///    "publicKey": [{
    ///         "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///         "type": "OpenIdConnectVersion1.0Service",
    ///         "owner": "OWNER!",
    ///         "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///     }],
    ///    "service": [{
    ///         "id": "id",
    ///         "type": "type",
    ///         "serviceEndpoint": "serviceEndpoint"
    ///     }],
    ///    "updated": "1982-02-02-00T00:00Z",
    ///    "created": "1982-02-02-00T00:00Z",
    ///    "proof": {
    ///        "type": "RsaSignature2018",
    ///        "created": "2018-06-17T10:03:48Z",
    ///        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///        "signatureValue": "pY9...Cky6Ed = "
    ///    }
    /// };
    /// let myDidDoc = DidDoc.fromJSON(myDidDoc_json);
    /// ```
    ///
    #[wasm_bindgen(js_name = fromJSON)]
    pub fn from_json(json: JsValue) -> DidDoc {
        DidDoc { ctx: json }
    }

    // SETTERS
    /// # Set context
    /// Specifications describe a `@context` as a mandatory field that:
    /// > one or more URIs (An identifier as defined by [RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986).)
    /// > where the value of the first URI is https://www.w3.org/2019/did/v1.
    /// > If more than one URI is provided, the URIs MUST be interpreted as an ordered set.
    /// > It is RECOMMENDED that dereferencing the URIs results in a document containing machine-readable information about
    /// > the context.
    ///
    /// And  must follow the following rules:
    ///
    /// 1. A DID Document MUST have exactly one top-level context statement.
    /// 2. The key for this property MUST be @context.
    /// 3. The value of this key MUST be the URL for the generic DID context: https://w3id.org/did/v1.
    ///
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     @context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `context`s use method `setContext()` can be used.
    /// ```javascript
    /// import { DidDoc } from 'diddoc';
    /// let vc = new DidDoc("my_id")
    ///     .setContext("new_context");
    /// ```
    ///
    #[wasm_bindgen(js_name = setContext)]
    pub fn set_context(self, c: String) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_context(c);
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Id (Did Subject)
    /// A DID subject goes by the following rules:
    ///
    /// 1. A DID Document MUST have exactly one DID subject.
    /// 2. The key for this property MUST be id.
    /// 3. The value of this key MUST be a valid DID.
    /// 4. When this DID Document is registered with the target Decentralized Identifier Registry, the registered DID MUST
    /// match this DID subject value.
    ///
    /// Similarly to `@context`, in `diddoc`, `id`s are created in the `DidDoc` constructor. Users can add an id subject using
    /// method `add_id()`, and can overwrite them using setter `set_id()`. `id`s are not array so no adder is implemented,
    /// only a setter and a getter.
    ///
    /// By default `id` will be set by the constructor.
    /// To overwrite default `id` use method `setId()` can be used.
    /// ```javascript
    /// import { DidDoc } from 'diddoc';
    /// let vc = new DidDoc("my_id")
    ///     .setId("_id");
    /// ```
    ///
    #[wasm_bindgen(js_name = setId)]
    pub fn set_id(self, id: String) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_id(id);
        DidDoc { ctx: d.to_json() }
    }

    /// # Set DID Authentication
    /// Following w3's documentation:
    ///
    /// > Authentication is the mechanism by which a DID subject can cryptographically prove that
    /// they are associated with a DID.
    ///
    /// The rules for Authentication are:
    ///
    /// 1. A DID Document MAY include an `authentication` property.
    /// 2. The value of the `authentication` property should be an array of verification methods.
    /// 3. Each verification method MAY be embedded or referenced. An example of a verification
    /// method is a public key.
    ///
    /// By default `authentication` is set to an empty object with empty properties (`type` and
    /// `publicKey`).
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setAuthentication({
    ///            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "type": "OpenIdConnectVersion1.0Service",
    ///            "owner": "OWNER!",
    ///            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setAuthentication)]
    pub fn set_authentication(self, a: JsValue) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_authentication(a.into_serde().unwrap());
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Public Key
    /// >Public keys are used for digital signatures, encryption and other cryptographic operations, which in turn are the
    /// basis for purposes such as authentication or establishing secure communication with service endpoints. In addition,
    /// public keys may play a role in authorization mechanisms of DID CRUD operations
    ///
    ///
    /// The rules for public keys are:
    ///
    /// 1. A DID Document MAY include a publicKey property.
    /// 2. The value of the `publicKey` property MUST be an array of public keys, and every public key property MUST be in the
    /// [Linked Data Cryptographic Suite Registry](https://w3c-ccg.github.io/ld-cryptosuite-registry/).
    /// 3. Each public key MUST include `id` and `type` properties, and exactly one value property. The array of public keys
    /// SHOULD NOT contain duplicate entries with the same id and different value properties with different formats.
    /// 4. Each public key MUST include a `controller` property, which identifies the controller of the corresponding private key.
    /// 5. A registry of key types and formats is available in [Registries](https://w3c-ccg.github.io/did-spec/#registries) .
    ///
    ///
    /// When creating a `publicKey` the following fields must be included (empty if not needed):
    ///
    /// 1. `id`
    /// 2. `owner`
    /// 3. `type`
    /// 4. A valid key-value (exactly one) encryption method as the key, and the public key corresponding to that encrpytion
    /// method, as the value.
    ///
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setPublicKey({
    ///         "id": "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
    ///         "type": "Ed25519VerificationKey2018",
    ///         "owner": "NEW_OWNER",
    ///         "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setPublicKey)]
    pub fn set_public_key(self, p: JsValue) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_public_key(p.into_serde().unwrap());
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Service
    /// As w3's documentation explains:
    /// > A service endpoint may represent any type of service the subject wishes to advertise, including decentralized
    /// identity management services for further discovery, authentication, authorization, or interaction.
    ///
    ///  The rules for service endpoints are:
    ///
    /// 1. A DID Document MAY include a service property.
    /// 2. The value of the service property should be an array of service endpoints.
    /// 3. Each service endpoint must include id, type, and serviceEndpoint properties, and MAY include additional properties.
    /// 4. The service endpoint protocol SHOULD be published in an open standard specification.
    /// 5. The value of the serviceEndpoint property MUST be a JSON-LD object or a valid URI conforming to
    /// [RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986) and normalized according to the rules in section 6 of
    /// [RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986) and to any normalization rules in its applicable URI
    /// scheme specification.
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setService({
    ///         "id": "did:ser:18y34b91c612c4123gwrh45y",
    ///         "type": "agent",
    ///         "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setService)]
    pub fn set_service(self, s: JsValue) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_service(s.into_serde().unwrap());
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Proof
    /// A proof on a DID Document is cryptographic proof of the integrity of the DID Document according to either:
    ///
    /// 1. The subject as defined as a [service endpoint](https://w3c-ccg.github.io/did-spec/#service-endpoints), or if
    /// not present:
    /// 2. The delegate as a [generic DID parameter](https://w3c-ccg.github.io/did-spec/#generic-did-parameter-names).
    ///
    /// This proof is NOT proof of
    /// [the binding between a DID and a DID Document](https://w3c-ccg.github.io/did-spec/#binding-of-identity).
    ///
    /// The rules for a proof are:
    ///
    /// 1. A DID Document MAY have exactly one property representing a proof.
    /// 2. The key for this property MUST be proof.
    /// 3. The value of this key MUST be a valid JSON-LD proof as defined by
    /// [Linked Data Proofs](https://w3c-dvcg.github.io/ld-signatures/).
    ///
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setProof({
    ///         "type": "RsaSignature2018",
    ///         "created": "2018-06-17T10:03:48Z",
    ///         "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///         "signatureValue": "pY9...Cky6Ed = "
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = setProof)]
    pub fn set_proof(self, proof: JsValue) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_proof(proof.into_serde().unwrap());
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Created
    /// Standard metadata for identifier records includes a timestamp of the original creation. The rules for including a
    /// creation timestamp are:
    ///
    /// 1. A DID Document MUST have zero or one property representing a creation timestamp. It is RECOMMENDED to include
    /// this property.
    /// 2. The key for this property MUST be created.
    /// 3. The value of this key MUST be a valid XML datetime value as defined in section 3.3.7 of
    /// [W3C XML Schema Definition Language (XSD) 1.1 Part 2: Datatypes](https://www.w3.org/TR/xmlschema11-2/)
    /// [[XMLSCHEMA11-2](https://w3c-ccg.github.io/did-spec/#bib-xmlschema11-2)].
    /// 4. This datetime value MUST be normalized to UTC 00:00 as indicated by the trailing "Z".
    /// 5. Method specifications that rely on DLTs SHOULD require time values that are after the known
    /// ["median time past" (defined in Bitcoin BIP 113)](https://github.com/bitcoin/bips/blob/master/bip-0113.mediawiki),
    /// when the DLT supports such a notion.
    ///
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setCreated("2002-10-10T17:00:00Z");
    /// ```
    ///
    #[wasm_bindgen(js_name = setCreated)]
    pub fn set_created(self, c: String) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_created(c);
        DidDoc { ctx: d.to_json() }
    }

    /// # Set Updated
    /// Standard metadata for identifier records includes a timestamp of the most recent change. The rules for including
    /// an updated timestamp are:
    ///
    /// 1. A DID Document MUST have zero or one property representing an updated timestamp. It is RECOMMENDED to include this property.
    /// 2. The key for this property MUST be updated.
    /// 3. The value of this key MUST be a valid XML datetime value as defined in section 3.3.7 of
    /// [W3C XML Schema Definition Language (XSD) 1.1 Part 2: Datatypes](https://www.w3.org/TR/xmlschema11-2/)
    /// [[XMLSCHEMA11-2](https://w3c-ccg.github.io/did-spec/#bib-xmlschema11-2)].
    /// 4. This datetime value MUST be normalized to UTC 00:00 as indicated by the trailing "Z".
    /// 5. Method specifications that rely on DLTs SHOULD require time values that are after the known
    /// ["median time past" (defined in Bitcoin BIP 113)](https://github.com/bitcoin/bips/blob/master/bip-0113.mediawiki),
    /// when the DLT supports such a notion.
    ///
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .setUpdated("2016-10-17T02:41:00Z");
    /// ```
    ///
    #[wasm_bindgen(js_name = setUpdated)]
    pub fn set_updated(self, u: String) -> DidDoc {
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.set_updated(u);
        DidDoc { ctx: d.to_json() }
    }

    // GETTERS
    /// # Get Context
    /// ```javascript
    /// let dContext = dd.getContext();
    /// ```
    ///
    #[wasm_bindgen(js_name = getContext)]
    pub fn get_context(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.context).unwrap()
    }

    /// # Get DID Identification
    /// ```javascript
    /// let dId = dd.getId();
    /// ```
    ///
    #[wasm_bindgen(js_name = getId)]
    pub fn get_id(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.id).unwrap()
    }

    /// # Get DID Authentication
    /// ```javascript
    /// let dAuthentication = dd.getAuthentication();
    /// ```
    ///
    #[wasm_bindgen(js_name = getAuthentication)]
    pub fn get_authentication(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.authentication).unwrap()
    }

    /// # Get Public Key
    /// ```javascript
    /// let dPublicKey = dd.getPublicKey();
    /// ```
    ///
    #[wasm_bindgen(js_name = getPublicKey)]
    pub fn get_public_key(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.public_key).unwrap()
    }

    /// # Get Services
    /// ```javascript
    /// let dServices = dd.getService();
    /// ```
    ///
    #[wasm_bindgen(js_name = getService)]
    pub fn get_service(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.service).unwrap()
    }

    /// # Get Proof
    /// ```javascript
    /// let dProof = dd.getProof();
    /// ```
    ///
    #[wasm_bindgen(js_name = getProof)]
    pub fn get_proof(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.proof).unwrap()
    }

    /// # Get DID Creation (Created)
    /// ```javascript
    /// let dCreated = dd.getCreated();
    /// ```
    ///
    #[wasm_bindgen(js_name = getCreated)]
    pub fn get_created(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.created).unwrap()
    }

    /// # Get DID last update (Updated)
    /// ```javascript
    /// let dUpdated = dd.getUpdated();
    /// ```
    ///
    #[wasm_bindgen(js_name = getUpdated)]
    pub fn get_updated(&self) -> JsValue {
        let dd: DidDocument = self.ctx.into_serde().unwrap();
        JsValue::from_serde(&dd.updated).unwrap()
    }

    // ADDERS
    /// # Add Authentication
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .addAuthentication({
    ///            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "type": "OpenIdConnectVersion1.0Service",
    ///            "owner": "OWNER!",
    ///            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = addAuthentication)]
    pub fn add_authentication(self, json_auth: JsValue) -> DidDoc {
        let auth: Authentication = json_auth.into_serde().unwrap();
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.add_authentication(auth);
        DidDoc { ctx: d.to_json() }
    }

    /// # Add Public Key
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .addPublicKey({
    ///         "id": "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
    ///         "type": "Ed25519VerificationKey2018",
    ///         "owner": "", // empty `owner`
    ///         "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = addPublicKey)]
    pub fn add_public_key(self, json_publickey: JsValue) -> DidDoc {
        let publickey: DidDocPublicKey = json_publickey.into_serde().unwrap();
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.add_public_key(publickey);
        DidDoc { ctx: d.to_json() }
    }

    /// # Add Service
    /// ```javascript
    /// let d = new DidDoc("my_context", "my_id")
    ///     .addService({
    ///         "id": "did:ser:18y34b91c612c4123gwrh45y",
    ///         "type": "agent",
    ///         "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    ///     })
    ///     .setService({
    ///         "id": "did:ser:18y34b91c612c4123gwrh45y",
    ///         "type": "agent",
    ///         "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    ///     });
    /// ```
    ///
    #[wasm_bindgen(js_name = addService)]
    pub fn add_service(self, json_service: JsValue) -> DidDoc {
        let service: Service = json_service.into_serde().unwrap();
        let mut d: DidDocument = self.ctx.into_serde().unwrap();
        d.add_service(service);
        DidDoc { ctx: d.to_json() }
    }

    /// # Generate JSON from Did Document
    /// ```javascript
    /// let my_json = dd.toJSON();
    /// ```
    ///
    #[wasm_bindgen(js_name = toJSON)]
    pub fn to_json(&self) -> JsValue {
        self.ctx.clone()
    }
}
