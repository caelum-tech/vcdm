//!
//! Definition of main functionalities (properties with examples, getters, adders, setters)
//!
//!
use serde::Deserialize;
use serde::Serialize;

use wasm_bindgen::prelude::*;

use crate::did_doc_authentication::Authentication;
use crate::did_doc_public_key::DidDocPublicKey;

//use core::fmt::Write;
use crate::proof::Proof;
use crate::service::Service;

#[derive(Serialize, Deserialize, Debug)]
pub struct DidDocument {
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "@context", deserialize = "@context"))]
    pub context: Vec<String>,
    #[serde(default = "empty_string")]
    pub id: String,
    #[serde(default = "Vec::new")]
    pub authentication: Vec<Authentication>,
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "publicKey", deserialize = "publicKey"))]
    pub public_key: Vec<DidDocPublicKey>,
    #[serde(default = "Vec::new")]
    #[serde(rename(serialize = "service", deserialize = "service"))]
    pub service: Vec<Service>,
    #[serde(default = "Proof::empty_proof")]
    pub proof: Proof,
    #[serde(default = "empty_string")]
    pub created: String,
    #[serde(default = "empty_string")]
    pub updated: String,
}

fn empty_string() -> String {
    "".to_string()
}

impl DidDocument {
    // CONSTRUCTORS
    /// # DidDocument Constructor
    /// `DidDoc`'s constructor will create an empty `DidDoc` object. An empty object
    /// is not a valid `DidDoc`.
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// ```
    pub fn new(context: String, id: String) -> Self {
        DidDocument {
            context: vec![context],
            id,
            authentication: Vec::new(),
            public_key: Vec::new(),
            service: Vec::new(),
            proof: Proof::empty_proof(),
            created: "".to_string(),
            updated: "".to_string(),
        }
    }

    // SETTERS
    /// # Set context
    /// Specifications describe a `@context` as a mandatory field that:
    /// > one or more URIs (An identifier as defined by [RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986).)
    /// > where the value of the first URI is https://www.w3.org/2019/did/v1.
    /// > If more than one URI is provided, the URIs MUST be interpreted as an ordered set.
    /// > It is RECOMMENDED that dereferencing the URIs results in a document containing machine-readable information about
    /// > the context.
    ///
    /// And  must follow the following rules:
    ///
    /// 1. A DID Document MUST have exactly one top-level context statement.
    /// 2. The key for this property MUST be @context.
    /// 3. The value of this key MUST be the URL for the generic DID context: https://w3id.org/did/v1.
    ///
    /// By default `context` will be set to
    /// ```json
    /// {
    ///     @context: [
    ///         "https://www.w3.org/2018/credentials/v1",
    ///         "https://www.w3.org/2018/credentials/examples/v1"
    ///     ]
    /// }
    /// ```
    /// To overwrite default `context`s use method `setContext()` can be used.
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// assert_eq!(&"my_context".to_string(), &dd.context[0]);
    /// dd.set_context("new_context".to_string());
    /// assert_eq!(&"new_context".to_string(), &dd.context[0]);
    /// ```
    ///
    pub fn set_context(&mut self, c: String) {
        self.context = vec![c]
    }

    /// # Set Id (Did Subject)
    /// A DID subject goes by the following rules:
    ///
    /// 1. A DID Document MUST have exactly one DID subject.
    /// 2. The key for this property MUST be id.
    /// 3. The value of this key MUST be a valid DID.
    /// 4. When this DID Document is registered with the target Decentralized Identifier Registry, the registered DID MUST
    /// match this DID subject value.
    ///
    /// Similarly to `@context`, in `diddoc`, `id`s are created in the `DidDoc` constructor. Users can add an id subject using
    /// method `add_id()`, and can overwrite them using setter `set_id()`. `id`s are not array so no adder is implemented,
    /// only a setter and a getter.
    ///
    /// By default `id` will be set by the constructor.
    /// To overwrite default `id` use method `setId()` can be used.
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// assert_eq!(&"my_id".to_string(), &dd.id);
    /// dd.set_id("new_id".to_string());
    /// assert_eq!(&"new_id".to_string(), &dd.id);
    /// ```
    ///
    pub fn set_id(&mut self, id: String) {
        self.id = id
    }

    /// # DidDocument Set Authentication
    /// `DidDoc`'s constructor will create an empty `DidDoc` object. An empty object
    /// is not a valid `DidDoc`.
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// use diddoc::did_doc_authentication::Authentication;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_authentication(
    ///     Authentication::PublicKey(serde_json::from_str(r#"{
    ///            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "type": "OpenIdConnectVersion1.0Service",
    ///            "owner": "OWNER!",
    ///            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///        }"#).unwrap())
    /// );
    /// ```
    ///
    pub fn set_authentication(&mut self, a: Authentication) {
        self.authentication = vec![a]
    }

    /// # Set Publc Key
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_public_key(
    ///     serde_json::from_str(r#"{
    ///          "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
    ///          "id": "keys-2",
    ///          "owner": "did:example:pqrstuvwxyz0987654321",
    ///          "type": "Ed25519VerificationKey2018"
    ///      }"#,).unwrap()
    /// );
    /// ```
    ///
    pub fn set_public_key(&mut self, p: DidDocPublicKey) {
        self.public_key = vec![p]
    }

    /// # Set Service
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_service(
    ///     serde_json::from_str(r#"{
    ///         "id": "",
    ///         "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
    ///         "type": "custom"
    ///     }"#).unwrap()
    /// );
    /// ```
    ///
    pub fn set_service(&mut self, s: Service) {
        self.service = vec![s]
    }

    /// # Set Proof
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_proof(
    ///     serde_json::from_str(r#"{
    ///          "type": "RsaSignature2018",
    ///          "created": "2018-06-17T10:03:48Z",
    ///          "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
    ///          "signatureValue": "pY9...Cky6Ed = "
    ///     }"#).unwrap()
    /// );
    /// ```
    ///
    pub fn set_proof(&mut self, p: Proof) {
        self.proof = p
    }

    /// # Set Created
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_created("2002-10-10T17:00:00Z".to_string());
    /// ```
    ///
    pub fn set_created(&mut self, c: String) {
        self.created = c
    }

    /// # Set Updated
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.set_updated("2002-10-10T17:00:00Z".to_string());
    /// ```
    ///
    pub fn set_updated(&mut self, u: String) {
        self.updated = u
    }

    // GETTERS
    /// # Get Context
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_context();
    /// ```
    ///
    pub fn get_context(&self) -> Vec<String> {
        self.context.clone()
    }

    /// # Get Id
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_id();
    /// ```
    ///
    pub fn get_id(&self) -> String {
        self.id.clone()
    }

    /// # Get Authentication
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_authentication();
    /// ```
    ///
    pub fn get_authentication(&self) -> Vec<Authentication> {
        self.authentication.clone()
    }

    /// # Get Public Key
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_public_key();
    /// ```
    ///
    pub fn get_public_key(&self) -> Vec<DidDocPublicKey> {
        self.public_key.clone()
    }

    /// # Get Services
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_service();
    /// ```
    ///
    pub fn get_service(&self) -> Vec<Service> {
        self.service.clone()
    }

    /// # Get Proofs
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_proof();
    /// ```
    ///
    pub fn get_proof(&self) -> Proof {
        self.proof.clone()
    }

    /// # Get Created
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_created();
    /// ```
    ///
    pub fn get_created(&self) -> String {
        self.created.clone()
    }

    /// # Get Updated
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// let _ = dd.get_updated();
    /// ```
    ///
    pub fn get_updated(&self) -> String {
        self.updated.clone()
    }

    // ADDERS
    /// # Add Context
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.add_context("new_context".to_string());
    /// ```
    ///
    pub fn add_context(&mut self, c: String) {
        self.context.push(c);
    }

    /// # Add Authentication
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// use diddoc::did_doc_authentication::Authentication;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.add_authentication(
    ///     Authentication::PublicKey(serde_json::from_str(r#"{
    ///            "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    ///            "type": "OpenIdConnectVersion1.0Service",
    ///            "owner": "OWNER!",
    ///            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    ///        }"#).unwrap())
    /// );
    /// ```
    ///
    pub fn add_authentication(&mut self, diddoc_auth: Authentication) {
        self.authentication.push(diddoc_auth);
    }

    /// # Add Public Key
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.add_public_key(
    ///     serde_json::from_str(r#"{
    ///         "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
    ///         "id": "keys-2",
    ///         "owner": "did:example:pqrstuvwxyz0987654321",
    ///         "type": "Ed25519VerificationKey2018"
    ///     }"#).unwrap()
    /// );
    /// ```
    ///
    pub fn add_public_key(&mut self, pk: DidDocPublicKey) {
        self.public_key.push(pk);
    }

    /// # Add Service
    /// ```rust
    /// use diddoc::did_document::DidDocument;
    /// let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    /// dd.add_service(
    ///     serde_json::from_str(r#"{
    ///        "id": "",
    ///        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
    ///        "type": "custom"
    ///     }"#).unwrap()
    /// );
    /// ```
    ///
    pub fn add_service(&mut self, s: Service) {
        self.service.push(s);
    }

    // GENERATORS
    /// # Generate `String` from Did Document
    /// ```javascript
    /// let my_string = dd.to_str();
    /// ```
    ///
    pub fn to_str(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    /// # Generate `JsValue` from Did Document
    /// ```javascript
    /// let my_json = dd.toJSON();
    /// ```
    ///
    pub fn to_json(&self) -> JsValue {
        JsValue::from_serde(&self).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_diddoc() {
        let mut should = DidDocument::new("my_context".to_string(), "my_id".to_string());
        should.set_proof(
            serde_json::from_str(
                r#"
        {
            "type": "RsaSignature2018",
            "created": "2018-06-17T10:03:48Z",
            "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
            "signatureValue": "pY9...Cky6Ed = "
        }
        "#,
            )
            .unwrap(),
        );
        let dd_auth: DidDocument = serde_json::from_str(
            &r#"
        {
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [],
            "publicKey": [],
            "service": [],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "",
            "updated": ""
        }
        "#,
        )
        .unwrap();

        assert_eq!(
            serde_json::to_string(&should).unwrap(),
            serde_json::to_string(&dd_auth).unwrap()
        );
    }
}
