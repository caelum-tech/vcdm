# **D**ecentralized **Id**entity **Doc**uments

The `diddoc` crate is aimed to help `javascript` and `rust` developers manage `DID docs` 
(**D**ecentralized **Id**entity **Doc**uments) in an easy manner.
Following [w3's specifications](https://w3c-ccg.github.io/did-spec/#did-documents) on DID
Documents this crate aims to be a [Resolver](https://w3c-ccg.github.io/did-spec/#did-resolvers).
This means it will create, read, update and delete all properties of a DID document. As well as
interacting with other DIDs and DID document, using proof verification.
Also, it will be able to interact with others on other Decentralized Identifier Registry.
This will can effectively be used to perform all the operations required of a CKMS
(cryptographic key management system), such as key registration, replacement, rotation,
recovery, and expiration.
More details in [w3's method specifications](https://w3c-ccg.github.io/did-spec/#did-methods).

## Installing `DidDoc` for `JavaScript`
###### From `npm`
Add it to your package.json:
```json
"devDependencies": {
    "diddoc": "^0.1.0",
   ...
  }
```
Imported in your code:
```javascript
import { DidDoc } from 'diddoc';
```

###### From gitlab
Clone or download `vcdm` repository
```sh
git clone ...
```

Go to `diddoc` directory and build the package using `wasm-pack` (make sure wasm-pack is installed)
```sh 
cd diddoc

wasm-pack build
```
This will generate a directory called `pkg` inside `diddoc`  directory. 

Add the generated package to your `package.json`;
```json
"devDependencies": {
    "diddoc": "file:../diddoc/pkg",
    ...
}
```
Now your ready to import it in your `JavaScript` code as usual:
```javascript
import { DidDoc } from 'diddoc';
```

## Installing `DidDoc` for `Rust`
###### From gitlab
Clone or download `vcdm` repository
```sh
git clone ...
```

Add it to your `Cargo.toml`:
```toml
...
[dependencies]
diddoc = {version = "0.1", path = "../diddoc"}
...
```

###### Using cargo
Add crate to your Cargo.toml
```toml
...
[dependencies]
diddoc = "0.1.0"
...

```
## Getting started with `JavaScript`
In Javascript the object `DidDoc` generates a new one for every method call.

###### Construct a `DidDoc` from JSON
In order to create a `DidDoc` object from a JSON, taking advantage of validation and methods, use method 
`DidDoc.from_json`: 

```javascript
import { DidDoc } from 'diddoc';
let my_json = {
    "@context": ["my_context"],
    "id": "my_id",
    "authentication": [
        {
            "type": "my_auth_type1",
            "publicKey": [
                "my_1st_diddoc_auth1",
                "my_2nd_diddoc_auth1"
            ]
        },
        {
            "type": "my_auth_type2",
            "publicKey": [
                "my_1st_diddoc_auth2",
                "my_2nd_diddoc_auth2"
            ]
        }
    ],
    "publicKey": [
        {
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
            "id": "keys-2",
            "owner": "did:example:pqrstuvwxyz0987654321",
            "type": "Ed25519VerificationKey2018"
        },
        {
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
            "id": "keys-2",
            "owner": "did:example:pqrstuvwxyz0987654321",
            "type": "Ed25519VerificationKey2018"
        }
    ],
    "service": [{
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273"
        "type": "custom"
    }],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let my_diddoc = DidDoc.fromJSON(my_json);
```

###### Construct a `DidDoc` step by step
Equivalently, you can also create a `DidDoc` object step by step.

```javascript
import { DidDoc } from 'diddoc';

let myDiddoc = new DidDoc("my_context", "my_id")
    .addAuthentication({
        "type": "my_auth_type1",
        "publicKey": [
            "my_1st_diddoc_auth1",
            "my_2nd_diddoc_auth1"
        ]
    })
    .addAuthentication({
        "type": "my_auth_type2",
        "publicKey": [
            "my_1st_diddoc_auth2",
            "my_2nd_diddoc_auth2"
        ]
    })
    .addPublicKey({
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    })
    .addPublicKey({
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    })
    .addService({
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273"
        "type": "custom"
    })
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });
```

## Getting started with `Rust`
In Rust the struct representing a DID document is called `DidDocument` unlike JavaScript. This and the fact that 
methods in Rust don't generate a new struct have to be taken into account when using this crate in the different 
languages.

```rust
extern crate diddoc;

use diddoc::did_document::DidDocument;

let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    dd.add_authentication(serde_json::from_str(r#"{
        "type": "my_auth_type1",
        "publicKey": [
          "my_1st_diddoc_auth1",
          "my_2nd_diddoc_auth1"
        ]
    }"#).unwrap());
    dd.add_authentication(serde_json::from_str(r#"{
        "type": "my_auth_type2",
        "publicKey": [
            "my_1st_diddoc_auth2",
            "my_2nd_diddoc_auth2"
        ]
    }"#).unwrap());
    dd.add_public_key(serde_json::from_str(r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#).unwrap());
    dd.add_public_key(serde_json::from_str(r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#).unwrap());
    dd.add_service(serde_json::from_str(r#"{
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
    }"#).unwrap());
    dd.set_proof(
        serde_json::from_str(
            r#"{
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            }"#,
        )
        .unwrap(),
    );

    let dd_j: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [
                {
                    "type": "my_auth_type1",
                    "publicKey": [
                        "my_1st_diddoc_auth1",
                        "my_2nd_diddoc_auth1"
                    ]
                },
                {
                    "type": "my_auth_type2",
                    "publicKey": [
                        "my_1st_diddoc_auth2",
                        "my_2nd_diddoc_auth2"
                    ]
                }
            ],
            "publicKey": [
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                },
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                    "type": "RsaSignature2018",
                    "created": "2018-06-17T10:03:48Z",
                    "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                    "signatureValue": "pY9...Cky6Ed = "
                }
        }"#
    ).unwrap();

    assert_eq!(dd.to_str(), dd_j.to_str());
```

## Resolvers
> A DID resolver is a software component with an API designed to accept requests for DID lookups and execute the 
> corresponding DID method to retrieve the authoritative DID Document. To be conformant with this specification, a DID 
> resolver:
> 
> SHOULD validate that a DID is valid according to its DID method specification, otherwise it should produce an error.
> 
> MUST conform to the requirements of the applicable DID method specification when performing DID resolution 
> operations.
> 
> SHOULD offer the service of verifying the integrity of the DID Document if it is signed.
> 
> MAY offer the service of returning requested properties of the DID Document.

## Roadmap

- [ x ] Basic CRUD implementation on properties.
- [ x ] Basic CRUD implementation on Optional properties.
- [ ] Basic interaction with IPFS.
- [ ] Verifying cryptographic proofs.
- [ ] Implementing important DID methods such as `did-erc725`, `did-btcr`, `did-sov`, and others.
- [ ] Helper to implement DID for own methods. 

## Contributing
Please, contribute to `diddoc`! The more the better! Feel free to to open an issue and/or contacting directly with the 
owner for any request or suggestion.

## Acknowledgment
This library is created to satisfy w3's specifications.

## Code of conduct
This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at 
[http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License
This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in 
[LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) respectively.
