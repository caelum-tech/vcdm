
# Properties

In order to create a DID document we first need to look what is necessary in the Json-LD. Following w3’s DID document
[documentation](https://w3c-ccg.github.io/did-spec/#did-documents) we have the following DID Document example:
```rust
{
  "id": "did:sov:CYQLsccvwhMTowprMjGjQ6",
  "service": [
    {
      "type": "custom",
      "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e27304f4f9c17655f1c598b75237664aff67e48a50edc51505a742985cf"
    }
  ],
  "authentication": [
    {
      "type": "Ed25519SignatureAuthentication2018",
      "publicKey": [
        "did:sov:CYQLsccvwhMTowprMjGjQ6#key-1",
        "did:sov:CYQLsccvwhMTowprMjGjQ6#key-2"
      ]
    }
  ],
  "publicKey": [
    {
      "id": "did:sov:CYQLsccvwhMTowprMjGjQ6#key-1",
      "type": "Ed25519VerificationKey2018",
      "publicKeyBase58": "CLFRfp2wa3ifbsVvdq52WcpEy7aujactsoqQgxkz7ZKR"
    }
  ],
  "@context": "https://w3id.org/did/v0.11"
}

```