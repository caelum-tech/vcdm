## Authentication (Optional)
Following w3's documentation:

> Authentication is the mechanism by which a DID subject can cryptographically prove that they are associated 
with a DID.


The rules for Authentication are:

1. A DID Document MAY include an `authentication` property.
2. The value of the `authentication` property should be an array of verification methods.
3. Each verification method MAY be embedded or referenced. An example of a verification method is a public key.

### Rust example

```rust
dd.add_authentication(
    serde_json::from_str(
        r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
        }"#
    ).unwrap()
);
```

Note that a `did` of type `String` is also a valid `authentication`
```rust
dd.add_authentication(
    serde_json::from_str("did:lrn:123412341234").unwrap()
);
```

Overwrite ALL `authentication`s:
```rust
dd.set_authentication(
    serde_json::from_str(
        r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_new_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
        }"#
    ).unwrap()
);
```

In order to get all authentications in the current DID document method `get_authentication()` can be used:

```rust
let current_authentications = dd.get_authentication();
```

### Javascript example

```javascript
let d = new DidDoc("my_context", "my_id")
    .addAuthentication({
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_new_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    })
    .addAuthentication("did:lrn:123412341234")
    .setAuthentication({
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_new_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    });
```
At the end of the code above, only one authentication is in the DID document. Method `setAuthentication()` is 
used after adding the first authentication element, this means it overwrites ALL authentications previously added.

In order to get all authentications in the current DID document method `getAuthentication()` can be used:

```javascript
let current_authentications = dd.getAuthentication();
```
