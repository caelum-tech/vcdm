## DID Subject
A DID subject goes by the following rules:

1. A DID Document MUST have exactly one DID subject.
2. The key for this property MUST be id.
3. The value of this key MUST be a valid DID.
4. When this DID Document is registered with the target Decentralized Identifier Registry, the registered DID MUST 
match this DID subject value.

Similarly to `@context`, in `diddoc`, `id`s are created in the `DidDoc` constructor. Users can add an id subject using 
method `add_id()`, and can overwrite them using setter `set_id()`. `id`s are not array so no adder is implemented, 
only a setter and a getter.

### Rust example
As said before a `DidDocument` struct, a `@context` and and `id` must be given. 

```rust 
let mut dd = DidDocument::new("my_context", "my_id");
```

Overwrite ALL contexts:
```rust 
dd.set_context("new_id");
```

Get DID document `id`:
```rust 
dd.get_id();
```

### Javascript example

```javascript 
var dd = new DidDoc("my_context", "my_id");
```

Add context to array:
```javascript 
dd = dd.addId("new_id");
```

Get DID document `id`:
```javascript 
let myId = dd.getId();
```
This will return:
```javascript
"my_id"
```
