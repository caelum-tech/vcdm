## Context
Specifications describe a `@context` as a mandatory field that:
> one or more URIs (An identifier as defined by [RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986).)
> where the value of the first URI is https://www.w3.org/2019/did/v1.
> If more than one URI is provided, the URIs MUST be interpreted as an ordered set.
> It is RECOMMENDED that dereferencing the URIs results in a document containing machine-readable information about
> the context.

And  must follow the following rules:

1. A DID Document MUST have exactly one top-level context statement.
2. The key for this property MUST be @context.
3. The value of this key MUST be the URL for the generic DID context: https://w3id.org/did/v1.

In crate `diddoc` context are created in the `DidDoc` constructor. Users can add contexts using method `add_context()`,
and can overwrite them using setter `set_context()`.

### Rust example
When constructing a `DidDocument` struct, a `@context` and and `id` must be given. The rest of properties will be empty, and
therefore it won't be a valid DID document yet.
```rust
let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
```

Add context to array:
```rust
dd.add_context("my_context".to_string());
```

Overwrite ALL contexts:
```rust
dd.set_context("my_context".to_string());
```

Get all contexts:
```javascript
dd.get_context();
```

### Javascript example

```javascript
var dd = new DidDoc("my_context", "my_id");
```

Add context to array:
```javascript
dd = dd.addContext("my_context");
```

Overwrite ALL contexts (in this case with `new_context`):
```javascript
dd = dd.setContext("new_context");
```

Get all contexts:
```javascript
let dContext = dd.getContext();
```
