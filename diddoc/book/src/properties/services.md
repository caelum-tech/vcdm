#### Service Endpoints (Optional)
As w3's documentation explains:
> A service endpoint may represent any type of service the subject wishes to advertise, including decentralized 
identity management services for further discovery, authentication, authorization, or interaction.

 The rules for service endpoints are:

1. A DID Document MAY include a service property.
2. The value of the service property should be an array of service endpoints.
3. Each service endpoint must include id, type, and serviceEndpoint properties, and MAY include additional properties.
4. The service endpoint protocol SHOULD be published in an open standard specification.
5. The value of the serviceEndpoint property MUST be a JSON-LD object or a valid URI conforming to 
[RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986) and normalized according to the rules in section 6 of 
[RFC3986](https://w3c-ccg.github.io/did-spec/#bib-rfc3986) and to any normalization rules in its applicable URI 
scheme specification.

###### Rust example

```rust
dd.add_service(
    serde_json::from_str(
        r#"{
            "id": "",
            "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
            "type": "custom"
        }"#
    ).unwrap()
);
```

Overwrite ALL `service`s:
```rust
dd.set_service(
    serde_json::from_str(
        r#"{
            "id": "",
            "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
            "type": "custom"
        }"#
    ).unwrap()
);
```
In order to get all services in the current DID document method `get_service()` can be used:

```rust
let current_services = dd.get_service();
```


###### Javascript example

```javascript
let d = new DidDoc("my_context", "my_id")
    .addService({
        "id": "did:ser:18y34b91c612c4123gwrh45y",
        "type": "agent",
        "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    })
    .setService({
        "id": "did:ser:18y34b91c612c4123gwrh45y",
        "type": "agent",
        "serviceEndpoint": "https://agents.danubeclouds.com/agent/WRfXPg8dantKVubE3HX8pw"
    });
```

In order to get all services in the current DID document method `getService()` can be used:

```javascript
let current_services = dd.getService();
```
