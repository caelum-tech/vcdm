
# Public Keys
>Public keys are used for digital signatures, encryption and other cryptographic operations, which in turn are the 
basis for purposes such as authentication or establishing secure communication with service endpoints. In addition, 
public keys may play a role in authorization mechanisms of DID CRUD operations 


The rules for public keys are:

1. A DID Document MAY include a publicKey property.
2. The value of the `publicKey` property MUST be an array of public keys, and every public key property MUST be in the 
[Linked Data Cryptographic Suite Registry](https://w3c-ccg.github.io/ld-cryptosuite-registry/).
3. Each public key MUST include `id` and `type` properties, and exactly one value property. The array of public keys 
SHOULD NOT contain duplicate entries with the same id and different value properties with different formats.
4. Each public key MUST include a `controller` property, which identifies the controller of the corresponding private key.
5. A registry of key types and formats is available in [Registries](https://w3c-ccg.github.io/did-spec/#registries) .


When creating a `publicKey` the following fields must be included (empty if not needed):

1. `id`
2. `owner`
3. `type`
4. A valid key-value (exactly one) encryption method as the key, and the public key corresponding to that encrpytion 
method, as the value.

**NOTE:** The following is a non-exhaustive list of public key properties used by the community: publicKeyPem, 
publicKeyJwk, publicKeyHex, publicKeyBase64, publicKeyBase58, publicKeyMultibase, ethereumAddress.


The algorithm to use when processing a publicKey property in a DID Document is:

1. Let value be the data associated with the publicKey property and initialize result to null.
2. If value is an object, the key material is embedded. Set result to value.
3. If value is a string, the key is included by reference. Assume value is a URL.
   1. Dereference the URL and retrieve the publicKey properties associated with the URL (e.g., process the publicKey property at the top-level of the dereferenced document).
   2. Iterate through each public key object.
      1. If the id property of the object matches value, set result to the object.
4. If result does not contain at least the id, type, and controller properties as well as any mandatory public cryptographic material, as determined by the result's type property, throw an error.


## Rust example

By default when constructing a `DidDocument` property `publicKey` is an empty `Vec`. They can be added and overwritten.

Adding a `publicKey` to your diddoc
```rust
let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
dd.add_public_key(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
            }"#
        ).unwrap()
    );
```

Setter overwrites ALL `publicKeys` 
```rust
let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
dd.set_public_key(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
            }"#
        ).unwrap()
    );
```

In order to get all public keys in the current DID document method `get_public_key()` can be used:

```rust
let current_authentications = dd.get_public_key();
```
## Javascript example

```javascript
let d = new DidDoc("my_context", "my_id")
    .addPublicKey({
        "id": "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
        "type": "Ed25519VerificationKey2018",
        "owner": "", // empty `owner`
        "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    })
    .setPublicKey({
        "id": "did:sov:WRfXPg8dantKVubE3HX8pw#key-1",
        "type": "Ed25519VerificationKey2018",
        "owner": "NEW_OWNER",
        "publicKeyBase58": "~P7F3BNs5VmQ6eVpwkNKJ5D"
    });
```

In order to get all public keys in the current DID document method `getPublicKey()` can be used:

```javascript
let current_authentications = dd.getPublicKey();
```
