## Proof (Optional)
A proof on a DID Document is cryptographic proof of the integrity of the DID Document according to either:

1. The subject as defined as a [service endpoint](https://w3c-ccg.github.io/did-spec/#service-endpoints), or if 
not present:
2. The delegate as a [generic DID parameter](https://w3c-ccg.github.io/did-spec/#generic-did-parameter-names).

This proof is NOT proof of 
[the binding between a DID and a DID Document](https://w3c-ccg.github.io/did-spec/#binding-of-identity).

The rules for a proof are:

1. A DID Document MAY have exactly one property representing a proof.
2. The key for this property MUST be proof.
3. The value of this key MUST be a valid JSON-LD proof as defined by 
[Linked Data Proofs](https://w3c-dvcg.github.io/ld-signatures/).

### Rust example

Overwrite `proof`:
```rust
dd.set_proof(
    serde_json::from_str(
        r#"{
            "type": "RsaSignature2018",
            "created": "2018-06-17T10:03:48Z",
            "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
            "signatureValue": "pY9...Cky6Ed = "
        }"#,
    )
    .unwrap(),
);
```

In order to get current `proof` in the current DID document method `get_proof()` can be used:

```rust
let current_proof = dd.get_proof();
```


### Javascript example

```javascript
let d = new DidDoc("my_context", "my_id")
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });
```

In order to get current `proof` in the current DID document method `getProof()` can be used:

```rust
let currentProof = dd.getProof();
```
