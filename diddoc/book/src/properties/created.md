
## Created (Optional)
Standard metadata for identifier records includes a timestamp of the original creation. The rules for including a 
creation timestamp are:

1. A DID Document MUST have zero or one property representing a creation timestamp. It is RECOMMENDED to include 
this property.
2. The key for this property MUST be created.
3. The value of this key MUST be a valid XML datetime value as defined in section 3.3.7 of 
[W3C XML Schema Definition Language (XSD) 1.1 Part 2: Datatypes](https://www.w3.org/TR/xmlschema11-2/) 
[[XMLSCHEMA11-2](https://w3c-ccg.github.io/did-spec/#bib-xmlschema11-2)].
4. This datetime value MUST be normalized to UTC 00:00 as indicated by the trailing "Z".
5. Method specifications that rely on DLTs SHOULD require time values that are after the known 
["median time past" (defined in Bitcoin BIP 113)](https://github.com/bitcoin/bips/blob/master/bip-0113.mediawiki), 
when the DLT supports such a notion.

### Rust example

Overwrite `created`:
```rust
dd.set_created("2002-10-10T17:00:00Z".to_string());
```

In order to get current `created` in the current DID document method `get_created()` can be used:

```rust
let current_created = dd.get_created();
```

### Javascript example
```javascript
let d = new DidDoc("my_context", "my_id")
    .setCreated("2002-10-10T17:00:00Z");
```

In order to get current `created` in the current DID document method `getCreated()` can be used:

```rust
let current_created = dd.getCreated();
```
