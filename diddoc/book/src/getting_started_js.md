## Getting Started with Javascript

### Installing `DidDoc` for JavaScript
##### From `npm`
Add it to your package.json:
```json
"devDependencies": {
    "diddoc": "^0.1.0",
   ...
  }
```
Imported in your code:
```javascript
import { DidDoc } from 'diddoc';
```

##### From gitlab
Clone or download `vcdm` repository
```sh
git clone ...
```

Go to `diddoc` directory and build the package using `wasm-pack` (make sure wasm-pack is installed)
```sh 
cd diddoc

wasm-pack build
```
This will generate a directory called `pkg` inside `diddoc`  directory. 

Add the generated package to your `package.json`;
```json
"devDependencies": {
    "diddoc": "file:../diddoc/pkg",
    ...
}
```
Now your ready to import it in your `JavaScript` code as usual:
```javascript
import { DidDoc } from 'diddoc';
```

### Usage with JavaScript
In Javascript the object `DidDoc` generates a new one for every method call.

###### Construct a `DidDoc` from JSON
In order to create a `DidDoc` object from a JSON, taking advantage of validation and methods, use method 
`DidDoc.from_json`: 

```javascript

import { DidDoc } from 'diddoc';
let my_json = {
    "@context": ["my_context"],
    "id": "my_id",
    "authentication": [
        {
            "type": "my_auth_type1",
            "publicKey": [
                "my_1st_diddoc_auth1",
                "my_2nd_diddoc_auth1"
            ]
        },
        {
            "type": "my_auth_type2",
            "publicKey": [
                "my_1st_diddoc_auth2",
                "my_2nd_diddoc_auth2"
            ]
        }
    ],
    "publicKey": [
        {
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
            "id": "keys-2",
            "owner": "did:example:pqrstuvwxyz0987654321",
            "type": "Ed25519VerificationKey2018"
        },
        {
            "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
            "id": "keys-2",
            "owner": "did:example:pqrstuvwxyz0987654321",
            "type": "Ed25519VerificationKey2018"
        }
    ],
    "service": [{
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273"
        "type": "custom"
    }],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let my_diddoc = DidDoc.from_json(my_json);
```
###### Construct a `DidDoc` step by step
Equivalently, you can also create a `DidDoc` object step by step.

```javascript
import { DidDoc } from 'diddoc';

let my_diddoc = new DidDoc("my_context", "my_id")
    .add_authentication({
        "type": "my_auth_type1",
        "publicKey": [
            "my_1st_diddoc_auth1",
            "my_2nd_diddoc_auth1"
        ]
    })
    .add_authentication({
        "type": "my_auth_type2",
        "publicKey": [
            "my_1st_diddoc_auth2",
            "my_2nd_diddoc_auth2"
        ]
    })
    .add_public_key({
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    })
    .add_public_key({
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    })
    .add_service({
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273"
        "type": "custom"
    })
    .set_proof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });
```
