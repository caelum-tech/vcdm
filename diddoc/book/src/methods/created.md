## Create
**Not implemented.**
> The DID method specification MUST specify how a client creates a DID and its associated DID Document on the 
Decentralized Identifier Registry, including all cryptographic operations necessary to establish proof of control.
