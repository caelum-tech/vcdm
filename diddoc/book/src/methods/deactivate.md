## Deactivate
**Not implemented.**
> Although a core feature of distributed ledgers is immutability, the DID method specification MUST specify how a 
client can deactivate a DID on the Decentralized Identifier Registry, including all cryptographic operations necessary 
to establish proof of deactivation.