## Contributing

Please, contribute to `diddoc`! The more the better! Feel free to to open an issue and/or contacting directly with the 
owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy w3's specifications.

## Code of conduct
This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at 
[http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License
This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in 
[LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) respectively.
