# **D**ecentralized **Id**entity **Doc**uments

The `diddoc` crate is aimed to help `javascript` and `rust` developers manage `DID docs` 
(**D**ecentralized **Id**entity **Doc**uments) in an easy manner.
Following [w3's specifications](https://w3c-ccg.github.io/did-spec/#did-documents) on DID
Documents this crate aims to be a [Resolver](https://w3c-ccg.github.io/did-spec/#did-resolvers).
This means it will create, read, update and delete all properties of a DID document. As well as
interacting with other DIDs and DID document, using proof verification.
Also, it will be able to interact with others on other Decentralized Identifier Registry.
This will can effectively be used to perform all the operations required of a CKMS
(cryptographic key management system), such as key registration, replacement, rotation,
recovery, and expiration.
More details in [w3's method specifications](https://w3c-ccg.github.io/did-spec/#did-methods).