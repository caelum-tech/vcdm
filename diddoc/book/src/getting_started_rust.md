## Getting started with Rust
### Installing `DidDoc` for Rust

###### From gitlab
Clone or download `vcdm` repository
```sh
git clone ...
```

Add it to your `Cargo.toml`:
```toml
...
[dependencies]
diddoc = {version = "0.1", path = "../diddoc"}
...
```

###### Using cargo
Add crate to your Cargo.toml
```toml
...
[dependencies]
diddoc = "0.1.0"
...
```

### Usage with Rust
In Rust the struct representing a DID document is called `DidDocument` unlike JavaScript. This and the fact that 
methods in Rust don't generate a new struct have to be taken into account when using this crate in the different 
languages.

```rust
extern crate diddoc;

use diddoc::did_document::DidDocument;

let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    dd.add_authentication(serde_json::from_str(r#"{
        "type": "my_auth_type1",
        "publicKey": [
          "my_1st_diddoc_auth1",
          "my_2nd_diddoc_auth1"
        ]
    }"#).unwrap());
    dd.add_authentication(serde_json::from_str(r#"{
        "type": "my_auth_type2",
        "publicKey": [
            "my_1st_diddoc_auth2",
            "my_2nd_diddoc_auth2"
        ]
    }"#).unwrap());
    dd.add_public_key(serde_json::from_str(r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#).unwrap());
    dd.add_public_key(serde_json::from_str(r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#).unwrap());
    dd.add_service(serde_json::from_str(r#"{
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
    }"#).unwrap());
    dd.set_proof(
        serde_json::from_str(
            r#"{
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            }"#,
        )
        .unwrap(),
    );

    let dd_j: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [
                {
                    "type": "my_auth_type1",
                    "publicKey": [
                        "my_1st_diddoc_auth1",
                        "my_2nd_diddoc_auth1"
                    ]
                },
                {
                    "type": "my_auth_type2",
                    "publicKey": [
                        "my_1st_diddoc_auth2",
                        "my_2nd_diddoc_auth2"
                    ]
                }
            ],
            "publicKey": [
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                },
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                    "type": "RsaSignature2018",
                    "created": "2018-06-17T10:03:48Z",
                    "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                    "signatureValue": "pY9...Cky6Ed = "
                }
        }"#
    ).unwrap();

    assert_eq!(dd.to_str(), dd_j.to_str());
```
