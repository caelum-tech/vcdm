## Roadmap

- [ ] Basic CRUD implementation on properties.
- [ ] Basic CRUD implementation on Optional properties.
- [ ] Verifying cryptographic proofs.
- [ ] Implementing important DID methods such as `did-erc725`, `did-btcr`, `did-sov`, and others.
- [ ] Helper to implement DID for own methods. 
