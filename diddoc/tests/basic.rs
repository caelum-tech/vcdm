extern crate diddoc;

use diddoc::did_doc_authentication::Authentication;
use diddoc::did_doc_public_key::DidDocPublicKey;
use diddoc::did_document::DidDocument;
use diddoc::proof::Proof;
use diddoc::service::Service;

#[test]
fn didddoc_construction() {
    let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    dd.add_authentication(Authentication::PublicKey(
        serde_json::from_str(
            r#"{
                "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "OWNER!",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    }"#,
        )
        .unwrap(),
    ));
    dd.add_authentication(Authentication::DID(
        "did:lrn:123412341234#my_id".to_string(),
    ));
    dd.add_public_key(
        serde_json::from_str(
            r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#,
        )
        .unwrap(),
    );
    dd.add_public_key(
        serde_json::from_str(
            r#"{
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
        "id": "keys-2",
        "owner": "did:example:pqrstuvwxyz0987654321",
        "type": "Ed25519VerificationKey2018"
    }"#,
        )
        .unwrap(),
    );
    dd.add_service(
        serde_json::from_str(
            r#"{
        "id": "",
        "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
        "type": "custom"
    }"#,
        )
        .unwrap(),
    );
    dd.set_proof(
        serde_json::from_str(
            r#"{
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            }"#,
        )
        .unwrap(),
    );
    dd.set_created("2002-10-10T17:00:00Z".to_string());
    dd.set_updated("2016-10-17T02:41:00Z".to_string());
    let dd_j: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                    "type": "OpenIdConnectVersion1.0Service",
                    "owner": "OWNER!",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                },
                "did:lrn:123412341234#my_id"
            ],
            "publicKey": [
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                },
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "2002-10-10T17:00:00Z",
            "updated": "2016-10-17T02:41:00Z"
        }"#,
    )
    .unwrap();

    assert_eq!(dd.to_str(), dd_j.to_str());
}

#[test]
fn diddoc_getters() {
    let mut dd: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [{
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                    "type": "OpenIdConnectVersion1.0Service",
                    "owner": "my_id",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                },
                "did:lrn:123412341234#my_id"
            ],
            "publicKey": [{
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
            }],
            "service": [{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "2002-10-10T17:00:00Z",
            "updated": "2016-10-17T02:41:00Z"
        }"#,
    )
    .unwrap();

    assert_eq!(&"my_context".to_string(), &dd.context[0]);
    dd.set_context("new_context".to_string());
    assert_eq!(&"new_context".to_string(), &dd.context[0]);

    assert_eq!(&"my_id".to_string(), &dd.id);
    dd.set_id("new_id".to_string());
    assert_eq!(&"new_id".to_string(), &dd.id);

    assert_eq!(
        serde_json::to_string(&dd.get_authentication()).unwrap(),
        serde_json::to_string(&dd.authentication).unwrap()
    );
    assert_eq!(
        serde_json::to_string(&dd.get_public_key()).unwrap(),
        serde_json::to_string(&dd.public_key).unwrap()
    );
    assert_eq!(
        serde_json::to_string(&dd.get_service()).unwrap(),
        serde_json::to_string(&dd.service).unwrap()
    );
    assert_eq!(
        serde_json::to_string(&dd.get_proof()).unwrap(),
        serde_json::to_string(&dd.proof).unwrap()
    );
    assert_eq!(
        serde_json::to_string(&dd.get_created()).unwrap(),
        serde_json::to_string(&dd.created).unwrap()
    );
    assert_eq!(
        serde_json::to_string(&dd.get_updated()).unwrap(),
        serde_json::to_string(&dd.updated).unwrap()
    );
}

#[test]
fn diddoc_setters() {
    let mut dd: DidDocument = serde_json::from_str(
        r#"{
            "@context": ["my_context"],
            "id": "my_id",
            "authentication": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
                    "type": "OpenIdConnectVersion1.0Service",
                    "owner": "my_id",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                },
                "did:lrn:123412341234#my_id"
            ],
            "publicKey": [{
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "id": "keys-2",
                    "owner": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018"
            }],
            "service": [{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }],
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "created": "2002-10-10T17:00:00Z",
            "updated": "2016-10-17T02:41:00Z"
        }"#,
    )
    .unwrap();
    // Context
    assert_eq!(&"my_context".to_string(), &dd.context[0]);
    dd.set_context("new_context".to_string());
    assert_eq!(&"new_context".to_string(), &dd.context[0]);
    // Id
    assert_eq!(&"my_id".to_string(), &dd.id);
    dd.set_id("new_id".to_string());
    assert_eq!(&"new_id".to_string(), &dd.id);
    // Authentication
    let mut auth_should: Vec<Authentication> = Vec::new();
    auth_should.push(
        serde_json::from_str(
            r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_new_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
            }"#,
        )
        .unwrap(),
    );
    dd.set_authentication(
        serde_json::from_str(
            r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_new_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
            }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&auth_should).unwrap(),
        serde_json::to_string(&dd.authentication).unwrap()
    );
    // Publick Key
    let mut pk_should: Vec<DidDocPublicKey> = Vec::new();
    pk_should.push(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "new_pk_base58",
                "id": "new_id",
                "owner": "new_owner",
                "type": "new_type"
             }"#,
        )
        .unwrap(),
    );
    dd.set_public_key(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "new_pk_base58",
                "id": "new_id",
                "owner": "new_owner",
                "type": "new_type"
             }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&pk_should).unwrap(),
        serde_json::to_string(&dd.public_key).unwrap()
    );
    // Service
    let mut service_should: Vec<Service> = Vec::new();
    service_should.push(
        serde_json::from_str(
            r#"{
                "id": "new_id",
                "serviceEndpoint": "new_serviceEndpoint",
                "type": "new_type"
            }"#,
        )
        .unwrap(),
    );
    dd.set_service(
        serde_json::from_str(
            r#"{
                "id": "new_id",
                "serviceEndpoint": "new_serviceEndpoint",
                "type": "new_type"
            }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&service_should).unwrap(),
        serde_json::to_string(&dd.service).unwrap()
    );
    // Poof
    let proof_should: Proof = serde_json::from_str(
        r#"{
            "type": "new_type",
            "created": "new_created",
            "verificationMethod": "new_verificationMethod",
            "signatureValue": "new_signatureValue"
        }"#,
    )
    .unwrap();
    dd.set_proof(
        serde_json::from_str(
            r#"{
                "type": "new_type",
                "created": "new_created",
                "verificationMethod": "new_verificationMethod",
                "signatureValue": "new_signatureValue"
            }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&proof_should).unwrap(),
        serde_json::to_string(&dd.proof).unwrap()
    );
    // Created
    dd.set_created("new_created".to_string());
    assert_eq!(
        serde_json::to_string(&"new_created".to_string()).unwrap(),
        serde_json::to_string(&dd.created).unwrap()
    );
    // Updated
    dd.set_updated("new_updated".to_string());
    assert_eq!(
        serde_json::to_string(&"new_updated".to_string()).unwrap(),
        serde_json::to_string(&dd.updated).unwrap()
    );
}

#[test]
fn diddoc_adders() {
    let mut dd = DidDocument::new("my_context".to_string(), "my_id".to_string());
    let mut auth_should: Vec<Authentication> = Vec::new();
    assert_eq!(
        serde_json::to_string(&auth_should).unwrap(),
        serde_json::to_string(&dd.get_authentication()).unwrap()
    );
    dd.add_authentication(
        serde_json::from_str(
            r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    }"#,
        )
        .unwrap(),
    );
    auth_should.push(
        serde_json::from_str(
            r#"{
                "id": "did:example:123412341234",
                "type": "OpenIdConnectVersion1.0Service",
                "owner": "my_id",
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&auth_should).unwrap(),
        serde_json::to_string(&dd.get_authentication()).unwrap()
    );

    let mut pk_should: Vec<DidDocPublicKey> = Vec::new();
    assert_eq!(
        serde_json::to_string(&pk_should).unwrap(),
        serde_json::to_string(&dd.get_public_key()).unwrap()
    );
    dd.add_public_key(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
             }"#,
        )
        .unwrap(),
    );
    pk_should.push(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
             }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&pk_should).unwrap(),
        serde_json::to_string(&dd.get_public_key()).unwrap()
    );
    pk_should.push(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
            }"#,
        )
        .unwrap(),
    );
    dd.add_public_key(
        serde_json::from_str(
            r#"{
                "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                "id": "keys-2",
                "owner": "did:example:pqrstuvwxyz0987654321",
                "type": "Ed25519VerificationKey2018"
            }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&pk_should).unwrap(),
        serde_json::to_string(&dd.get_public_key()).unwrap()
    );
    let mut service_should: Vec<Service> = Vec::new();
    assert_eq!(
        serde_json::to_string(&service_should).unwrap(),
        serde_json::to_string(&dd.get_service()).unwrap()
    );
    service_should.push(
        serde_json::from_str(
            r#"{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }"#,
        )
        .unwrap(),
    );
    dd.add_service(
        serde_json::from_str(
            r#"{
                "id": "",
                "serviceEndpoint": "https://notary.ownyourdata.eu/?hash=fef73e273",
                "type": "custom"
            }"#,
        )
        .unwrap(),
    );
    assert_eq!(
        serde_json::to_string(&service_should).unwrap(),
        serde_json::to_string(&dd.get_service()).unwrap()
    );
}
