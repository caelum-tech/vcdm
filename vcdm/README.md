# **V**erifiable **C**redential **D**ata **M**odel [WIP]

This crates implements w3's [Verifiable Credential Data Model](https://www.w3.org/TR/verifiable-claims-data-model/)
specifications.
This means it will create, read, update and delete (a.k.a. CRUD) all properties of a
`Verifiable Credential`and a `Verifiable Presentation`. As well as interacting with other
`Verifiable Credential` and `Verifiable Presentation`.

> A `Verifiable Credential` or a `Verifiable Presentation`  can represent all of the same 
information that a physical credential represents. The addition of technologies, such as 
digital signatures, makes verifiable credentials more tamper-evident and more trustworthy 
than their physical counterparts.

> In the physical world, a credential might consist of:
 - Information related to identifying the subject of the credential (for example, a photo, name, 
or identification number)
 - Information related to the issuing authority (for example, a city government, national 
agency, or certification body)
 - Information related to the type of credential this is (for example, a Dutch passport, an 
American driving license, or a health insurance card)
 - Information related to specific attributes or properties being asserted by the issuing 
authority about the subject (for example, nationality, the classes of vehicle entitled to 
drive, or date of birth)
 - Evidence related to how the credential was derived
 - Information related to constraints on the credential (for example, expiration date, or terms 
of use).

We want to help developers create these scenarios and making it easy to interact with other 
following the same specifications.

## Getting started with `JavaScript`


```javascript
import {
    VPresentation,
    VCredential
} from 'vcdm';

// Contructing a Verifiable Credential
let vc = new VCredential("my_id")
    // Setters overwrite values of property
    .setIssuanceDate("2010-01-01T19:73:24Z")
    .setIssuer("Issuer")
    .setCredentialSubject({"credentialSubject": {
        "type": "parental_admission",
        "name": "parent_name",
        "field_trip": "Sagrada Familia",
        "permission": "true",
    }})
    // Setting another credential totally different and defined by user
    .setSredentialSubject({"credentialSubject": {
        "type": "my_id",
        "name": "my_name",
        "mnumber": "my_number",
        "address": "my_address",
        "birthDate": "my_birth_date",
    }})
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    })
    // Adders push values to the vector properties
    .addContext("added context")
    .addType("added type")
    .addPublicKey({
        "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
        "type": "OpenIdConnectVersion1.0Service",
        "controller": "CONTROLLER!",
        "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
    })
    .addService({
        "id": "did:ser:18y34b91c612c4123gwrh45y",
        "type": "service_type",
        "serviceEndpoint": "www.caelumlabs.com/api/"
    });

console.log(vc.toJSON());

// Contructing a Verifiable Presentation
let vp = new VPresentation("my_id")
    .addContext("added Context")
    .addType("added type")
    // Add the created Verifiable Credential
    .addVerifiableCredential(vc.toJSON())
    .setProof({
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    });

console.log(vp.toJSON());
```

or...

```javascript
import { VPresentation } from 'vcdm';

let vp_json = {
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "type": [
        "VerifiableCredential",
        "PersonalInformation"
    ],
    "verifiableCredential": [
        {
            "@context": [
                "https://www.w3.org/2018/credentials/v1",
                "https://www.w3.org/2018/credentials/examples/v1"
            ],
            "id": "http://example.com/credentials/4643",
            "type": [
                "VerifiableCredential",
                "PersonalInformation"
            ],
            "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
            "issuanceDate": "2010-01-01T19:73:24Z",
            "credentialSubject": {
                "type": "did:example:abfab3f512ebc6c1c22de17ec77",
                "name": "Mr John Doe",
                "mnumber": "77373737373A",
                "address": "10 Some Street, Anytown, ThisLocal, Country X",
                "birthDate": "1982-02-02-00T00:00Z"
            },
            "proof": {
                "type": "RsaSignature2018",
                "created": "2018-06-17T10:03:48Z",
                "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
                "signatureValue": "pY9...Cky6Ed = "
            },
            "publicKey": [
                {
                    "id": "did:example:ebfeb1276e12ec21f712ebc6f1c#keys-2",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "type": "Ed25519VerificationKey2018",
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV"
                }
            ],
            "authentication": [
                "did:example:ebfeb1276e12ec21f712ebc6f1c",
                {
                    "PublicKeyBase58": "H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV",
                    "controller": "did:example:pqrstuvwxyz0987654321",
                    "id": "keys-2",
                    "type": "Ed25519VerificationKey2018"
                }
            ],
            "service": [
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                },
                {
                    "id": "openid",
                    "type": "OpenIdConnectVersion1.0Service",
                    "serviceEndpoint": "https://openid.example.com/"
                }
            ]
        }
    ],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
};

let vp = VPresentation.fromJSON(vp_json);

console.log(vp.toJSON());
```

## Getting started with `Rust`
```rust
let vc_json_str = r#"{
    "@context": [
        "https://www.w3.org/2018/credentials/v1",
        "https://www.w3.org/2018/credentials/examples/v1"
    ],
    "id": "http://example.com/credentials/4643",
    "type": [
        "VerifiableCredential",
        "PersonalInformation"
    ],
    "issuer": "did:example:ebfeb1276e12ec21f712ebc6f1c",
    "issuanceDate": "2010-01-01T19:73:24Z",
    "credentialStatus": {
        "id": "cred_stat_id",
        "type": "cred_stat_credential_subject_type"
    },
    "credentialSubject": [{
        "type": "did:example:abfab3f512ebc6c1c22de17ec77",
        "name": "Mr John Doe",
        "mnumber": "77373737373A",
        "address": "10 Some Street, Anytown, ThisLocal, Country X",
        "birthDate": "1982-02-02-00T00:00Z"
    }],
    "proof": {
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }
}"#;

let _ver_cred_from_json: VerifiableCredential = serde_json::from_str(vc_json_str).unwrap();
```
Equivalent to:
```rust
let mut ver_cred_vcdm =
            VerifiableCredential::new("http://example.com/credentials/4643".to_string());
ver_cred_vcdm.set_proof(
    serde_json::from_str(r#"{
        "type": "RsaSignature2018",
        "created": "2018-06-17T10:03:48Z",
        "verificationMethod": "did:example:ebfeb1276e12ec21f712ebc6f1c#k1",
        "signatureValue": "pY9...Cky6Ed = "
    }"#).unwrap()
);
ver_cred_vcdm.set_credential_subject(
    serde_json::from_str(r#"{
        "type": "did:example:abfab3f512ebc6c1c22de17ec77",
        "name": "Mr John Doe",
        "mnumber": "77373737373A",
        "address": "10 Some Street, Anytown, ThisLocal, Country X",
        "birthDate": "1982-02-02-00T00:00Z"
    }"#).unwrap()
);
ver_cred_vcdm.set_issuance_date("2010-01-01T19:73:24Z".to_string());
ver_cred_vcdm.set_issuer("did:example:ebfeb1276e12ec21f712ebc6f1c".to_string());
ver_cred_vcdm.set_credential_status(
    serde_json::from_str(r#"{
        "id": "cred_stat_id",
        "type": "cred_stat_credential_subject_type"
    }"#).unwrap(),
);
```

## Roadmap

- [ X ] Basic CRUD implementation on properties.
- [ ] Basic CRUD implementation on Optional properties.
- [ ] Implementation of Roles (`Issuer`, `Holder`, `Verifier`, etc.).
- [ ] Connecting to `Verifiable Data Registry`.
- [ ] Validation for all properties.
- [ ] Verifying basic cryptographic proofs.
- [ ] Implementation of Zero-Knowledge proof.

## Contributing

Please, contribute to `vcdm`! The more the better! Feel free to to open an issue and/or contacting directly with the 
owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy w3's specifications.

## Code of conduct
This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at 
[http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License
This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in 
[LICENSE-APACHE](LICENSE-APACHE) and [LICENSE-MIT](LICENSE-MIT) respectively.
