use serde::{Deserialize, Serialize};

use std::collections::HashMap;
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CredSubject {
    #[serde(
        flatten,
        rename(serialize = "credentialSubject", deserialize = "credentialSubject")
    )]
    pub cred_subj: HashMap<String, String>,
}

impl CredSubject {
    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn new(cs: HashMap<String, String>) -> Self {
        CredSubject { cred_subj: cs }
    }
}
