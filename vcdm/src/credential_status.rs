use serde::{Deserialize, Serialize};
use wasm_bindgen::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CredentialStatus {
    id: String,
    #[serde(rename(serialize = "type", deserialize = "type"))]
    credential_status_type: String,
}

impl Default for CredentialStatus {
    fn default() -> Self {
        CredentialStatus {
            id: "".to_string(),
            credential_status_type: "".to_string(),
        }
    }
}

impl CredentialStatus {
    pub fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }

    pub fn new(id: String, status_type: String) -> Self {
        CredentialStatus {
            id,
            credential_status_type: status_type,
        }
    }
}
