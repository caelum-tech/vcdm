## Key concepts
In the following section several important keywords are explained in a simple manner. Exmaple of claim 
use cases can be found [here](https://www.w3.org/TR/verifiable-claims-use-cases/).

**holder**
> A role an *entity* might perform by possessing one or more *verifiable credentials* and generating *verifiable 
presentations* from them. Example holders include students, employees, and customers.

**issuer**
> A role an *entity* performs by asserting *claims* about one or more *subjects*, creating a *verifiable credential*
from these *claims*, and transmitting the *verifiable credential* to a *holder*. Example issuers include corporations, 
non-profit organizations, trade associations, governments, and individuals.

**subject**
> An *entity* about which *claims* are made. Example subjects include human beings, animals, and things. In many 
cases the *holder* of a *verifiable credential* is the subject, but in certain cases it is not. For example, a 
parent (the *holder*) might hold the *verifiable credentials* of a child (the *subject*), or a pet owner (the *holder*) 
might hold the *verifiable credentials* of their pet (the *subject*).

**verifier**
> A role an entity performs by receiving one or more *verifiable credentials*, optionally inside a *verifiable 
presentation*, for processing. Example verifiers include employers, security personnel, and websites.

**verifiable data registry**
> A role a system might perform by mediating the creation and verification of identifiers, keys, and other 
relevant data, such as verifiable credential schemas, revocation registries, issuer public keys, and so on, 
which might be required to use *verifiable credentials*. Some configurations might require correlatable identifiers
for *subjects*. Example verifiable data registries include trusted databases, decentralized databases, government 
ID databases, and distributed ledgers. Often there is more than one type of verifiable data registry utilized 
in an ecosystem.

![](https://www.w3.org/TR/vc-data-model/diagrams/ecosystem.svg)

**claim**
> An assertion made about a *subject*.

**credential**
> A set of one or more claims made by an *issuer*. A *verifiable credential* is a tamper-evident credential 
>that has authorship that can be cryptographically verified. Verifiable credentials can be used to build 
>*verifiable presentations*, which can also be cryptographically verified. The *claims* in a credential can be 
>about different *subjects*.

**data minimization**
> The act of limiting the amount of shared data strictly to the minimum necessary to successfully accomplish
> a task or goal.

**decentralized identifier**
> A portable URL-based identifier, also known as a DID, associated with an entity. These identifiers are most 
>often used in a *verifiable credential* and are associated with *subjects* such that a *verifiable credential* itself 
>can be easily ported from one repository to another without the need to reissue the *credential*. An example of a 
>DID is did:example:123456abcdef.

**decentralized identifier document**
> Also referred to as a DID document, this is a document that is accessible using a *verifiable data registry* 
>and contains information related to a specific decentralized identifier, such as the associated repository 
>and public key information.

**derived predicate**
> A verifiable, boolean assertion about the value of another attribute in a *verifiable credential*. These are
> useful in zero-knowledge-proof-style *verifiable presentations* because they can limit information disclosure. 
>For example, if a *verifiable credential* contains an attribute for expressing a specific height in centimeters, 
>a derived predicate might reference the height attribute in the *verifiable credential* demonstrating that the 
>*issuer* attests to a height value meeting the minimum height requirement, without actually disclosing the specific 
>height value. For example, the *subject* is taller than 150 centimeters.

**digital signature**
> A mathematical scheme for demonstrating the authenticity of a digital message.

**entity**
> A thing with distinct and independent existence, such as a person, organization, or device that performs one 
>or more roles in the ecosystem.

**graph**
> A network of information composed of *subjects* and their relationship to other *subjects* or data.

**identity**
> The means for keeping track of entities across contexts. Digital identities enable tracking and customization 
>of entity interactions across digital contexts, typically using identifiers and attributes. Unintended distribution 
>or use of identity information can compromise privacy. Collection and use of such information should follow the 
>principle of data minimization.

**identity provider**
> An identity provider, sometimes abbreviated as IdP, is a system for creating, maintaining, and managing identity 
>information for holders, while providing authentication services to relying party applications within a federation 
>or distributed network. In this case the holder is always the *subject*. Even if the *verifiable credentials* are 
>bearer credentials, it is assumed the *verifiable credentials* remain with the *subject*, and if they are not, they 
>were stolen by an attacker. This specification does not use this term unless comparing or mapping the concepts in 
>this document to other specifications. This specification decouples the identity provider concept into two distinct
> concepts: the *issuer* and the holder.

**presentation**
> Data derived from one or more *verifiable credentials*, issued by one or more *issuers*, that is shared with a 
>specific verifier. A *verifiable presentation* is a tamper-evident presentation encoded in such a way that 
>authorship of the data can be trusted after a process of cryptographic verification. Certain types of 
>*verifiable presentations* might contain data that is synthesized from, but do not contain, the original 
>*verifiable credentials* (for example, zero-knowledge proofs).

**repository**
> A program, such as a storage vault or personal *verifiable credential* wallet, that stores and protects 
>access to holders' *verifiable credentials*.

**selective disclosure**
> The ability of a holder to make fine-grained decisions about what information to share.

**user agent**
> A program, such as a browser or other Web client, that mediates the communication between holders, 
>*issuers*, and verifiers.

**validation**
> The assurance that a *verifiable credential* or a *verifiable presentation* meets the needs of a verifier 
>and other dependent stakeholders. This specification is constrained to verifying *verifiable credentials* 
>and *verifiable presentations* regardless of their usage. Validating *verifiable credentials* or verifiable 
>presentations is outside the scope of this specification.

**verification**
> The evaluation of whether a *verifiable credential* or *verifiable presentation* complies with this specification.

**URI**
> A Uniform Resource Identifier, as defined by [RFC3986](https://tools.ietf.org/html/rfc3986).