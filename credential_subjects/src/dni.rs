use serde::{Deserialize, Serialize};

use wasm_bindgen::prelude::*;

use super::CredentialSubject;

#[derive(Serialize, Deserialize)]
pub struct DniCredSubject {
    #[serde(rename(serialize = "type", deserialize = "type"))]
    id: String,
    name: String,
    #[serde(rename(serialize = "mnumber", deserialize = "mnumber"))]
    number: String,
    address: String,
    #[serde(rename(serialize = "birthDate", deserialize = "birthDate"))]
    birth_date: String,
}

impl DniCredSubject {
    pub fn validate_cred_subject(_a: &DniCredSubject) -> bool {
        true
    }

    pub fn new(
        id: String,
        name: String,
        number: String,
        address: String,
        birth_date: String,
    ) -> Self {
        DniCredSubject {
            id,
            name,
            number,
            address,
            birth_date,
        }
    }
}

impl<'de> CredentialSubject<'de> for DniCredSubject {
    fn empty_subject() -> DniCredSubject {
        DniCredSubject {
            id: "".to_string(),
            name: "".to_string(),
            number: "".to_string(),
            address: "".to_string(),
            birth_date: "".to_string(),
        }
    }

    fn from_json(json: JsValue) -> Self {
        json.into_serde().unwrap()
    }
}
