use wasm_bindgen::prelude::*;

pub mod dni;

pub trait CredentialSubject<'de>: serde::Serialize + serde::Deserialize<'de> {
    //TODO: Could change this to Default
    fn empty_subject() -> Self;
    //TODO: Add validators
    fn from_json(json: JsValue) -> Self;
}
