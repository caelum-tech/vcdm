#!/usr/bin/env bash

# Run this script in project folder

cargo test --no-run

for x in $(find ./target/debug/ -regex "./target/debug/[a-zA-Z0-9]*-[a-zA-Z0-9]*" -exec echo {} \;); do
    echo "Coverage for ${x#./target/debug/}:" $(cat target/cov/${x#./target/debug/}.*/coverage.json | grep percent_covered  | tail -n 1)
done

echo "Coverage for merged: " $(cat target/cov/kcov-merged/coverage.json | grep percent_covered  | tail -n 1)