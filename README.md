# VCDM
These rust crates are aimed to give support to JavaScript and Rust SSI (Self Sovereign Identity) applications. 

## Usage
This crate is available in a rust version using `cargo` tool ( in [crates.io](https://crates.io/)) and in JavaScript
using [npm](https://www.npmjs.com/) package manager.

If prefered clone this project directly:
```bash
git clone ...
```

This project is separated into two libraries/crates, `diddoc` and `vcdm`. Both of them follow as much as possible 
w3's documentation on [DID Documents](https://w3c-ccg.github.io/did-spec/) and 
[Verifiable Credential Data Model](https://www.w3.org/TR/verifiable-claims-data-model/).

###### `diddoc`
This crate gives support in order to manage DID Documents. A full description of this crate in *./diddoc/README.md*
or in the `diddoc` book.

###### `vcdm` 
This crate gives support for Verifiable Credential Data Model. A full description of this crate in *./vcdm/README.md*
or in the `vcdm` book.

### Prerequisites
Install  `npm`, `nodejs`, `openssl dev`, and `pkg-config`.


### Build crates from gitlab
Clone the project.
```sh
git clone https://gitlab.com/caelum-tech/vcdm.git
```
or
```sh
git clone git@gitlab.com:caelum-tech/vcdm.git
```

##### Build a WebAssembly library
Install `wasm-pack`
```sh
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
```

Setup WebAssembly:
```sh
cargo install cargo-generate
cargo install wasm-bindgen-cli
rustup toolchain install nightly
rustup target add wasm32-unknown-unknown --toolchain nightly
cargo +nightly build --target wasm32-unknown-unknown

```

Go to the crate you want to use (or both) and build a wasm package:
```sh
cd didoc # or cd vcdm
wasm-pack build
```
This will generate your wasm library in folder `./pkg`. Now add it to your `package.json`:
```json
"devDependencies": {
    ...
    "diddoc": "file:../path/to/.../diddoc/pkg",
    "vcdm": "file:../path/to/.../vcdm/pkg"
    ...
}
```

##### See changes
Once everything is setup use the following commands for any change done.

1. In main project directory (`vcdm`):
```sh
wasm-pack build
```
2. Go inside `www` directory:
```sh
cd wwww
npm install
npm run start
```

## Roadmap
 - [ ] Basic CRUD implementation on properties
 - [ ] Basic CRUD implementation on Optional properties.
 - [ ] Using cryptographic primitives to validate proofs (*zkp*, public key exchange *Diffie-Hellman* with *RSA* and *Elliptic-curve*).
 - [ ] Implementation of Roles (`Issuer`, `Holder`, `Verifier`).
 - [ ] Connecting to `Verifiable Data Registry`.
 - [ ] Validation for all properties.
 - [ ] Hashlink sub-crate.
 - [ ] Implementing important DID methods such as `did-erc725`, `did-btcr`, `did-sov`, and others.
 - [ ] Helper to implement DID for own methods. 
 
## Contributing
Please, contribute to `vcdm`! The more the better! Feel free to to open an issue and/or contacting directly with the 
owner for any request or suggestion.

## Acknowledgment

This library is created to satisfy w3's specifications.

## Code of conduct
This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4, available at 
[http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/

## License
This project is distributed under the terms of both the Apache License (Version 2.0) and the MIT license, specified in 
[LICENSE-APACHE](./LICENSE-APACHE) and [LICENSE-MIT](./LICENSE-MIT) respectively.
